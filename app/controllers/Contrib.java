package controllers;

import play.mvc.*;
import views.html.*;

public class Contrib extends Controller {

    public Result index(){
        return ok(main_contrib.render());
    }
}