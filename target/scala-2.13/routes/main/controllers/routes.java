// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/W.I. TEIXEIRA/Desktop/estag-contrib/conf/routes
// @DATE:Mon Apr 20 15:59:26 BRT 2020

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseContrib Contrib = new controllers.ReverseContrib(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseContrib Contrib = new controllers.javascript.ReverseContrib(RoutesPrefix.byNamePrefix());
  }

}
