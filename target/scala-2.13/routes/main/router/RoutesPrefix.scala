// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/W.I. TEIXEIRA/Desktop/estag-contrib/conf/routes
// @DATE:Mon Apr 20 15:59:26 BRT 2020


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
